# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 16:02:20 2022

@author: lab2


Largely a collection of wrapers for instruments

"""
from typing import Sequence, AbstractSet, Optional, Tuple, Mapping, Any, Union
import atsaverage
from atsaverage import alazar
from atsaverage.config import ScanlineConfiguration, CaptureClockConfiguration, EngineTriggerConfiguration,\
    TRIGInputConfiguration, InputConfiguration
import atsaverage.server
import atsaverage.client
import atsaverage.core
from qupulse.hardware.dacs import AlazarCard
from qupulse.hardware.setup import HardwareSetup,PlaybackChannel,MarkerChannel, MeasurementMask
from qupulse.hardware.awgs.zihdawg import HDAWGRepresentation, HDAWGChannelGrouping
from mux_qcodes_driver import Muxi
import itertools
#%%
def get_alazar():
    
    r = 2.5
    rid = alazar.TriggerRangeID.etr_TTL

    trig_level = int((r + 0.15) / (2*r) * 255)
    assert 0 <= trig_level < 256

    config = ScanlineConfiguration()
    config.triggerInputConfiguration = TRIGInputConfiguration(triggerRange=rid)
    config.triggerConfiguration = EngineTriggerConfiguration(triggerOperation=alazar.TriggerOperation.J,
                                                             triggerEngine1=alazar.TriggerEngine.J,
                                                             triggerSource1=alazar.TriggerSource.external,
                                                             triggerSlope1=alazar.TriggerSlope.positive,
                                                             triggerLevel1=trig_level,
                                                             triggerEngine2=alazar.TriggerEngine.K,
                                                             triggerSource2=alazar.TriggerSource.disable,
                                                             triggerSlope2=alazar.TriggerSlope.positive,
                                                             triggerLevel2=trig_level)
    
    config.captureClockConfiguration = CaptureClockConfiguration(source=alazar.CaptureClockType.external_clock,
                                                                 samplerate=alazar.SampleRateID.rate_100MSPS)
    config.inputConfiguration = 4*[InputConfiguration(input_range=alazar.InputRangeID.range_1_V)]
    # config.totalRecordSize = 0
    # config.aimedBufferSize = 10*config.aimedBufferSize

    # is set automatically
    assert config.totalRecordSize == 0
    
    config.autoExtendTotalRecordSize = 0
    config.AUTO_EXTEND_TOTAL_RECORD_SIZE = 0
    alazar_DAC = AlazarCard(atsaverage.core.getLocalCard(1, 1),config)
    #alazar_DAC.card.
    alazar_DAC.card.triggerTimeout = 1200000
    alazar_DAC.card.acquisitionTimeout = 1200000
    alazar_DAC.card.computationTimeout = 1200000
    
    #alazar_DAC.config.rawDataMask =atsaverage.atsaverage.ChannelMask(0)
    
    # channels=['A','B','C','D']
    # for i,channel in enumerate(channels):
    #      alazar_DAC.register_mask_for_channel(channel, i)

    return(alazar_DAC)

#%%

def get_hdawg(hardware_setup,Serial='DEV8493'):

    
    idx=str(len(hardware_setup.known_awgs))
    hdawg= HDAWGRepresentation(Serial, 'USB')
    hdawg.reset()
    hdawg._api_session
    channels, = hdawg.channel_tuples
    
    
    for ch_i, ch_name in enumerate('ABCDEFGH'):
        
        playback_name = 'ZI'+idx+'_{ch_name}'.format(ch_name=ch_name)
        hardware_setup.set_channel(playback_name, PlaybackChannel(channels, ch_i))
        hardware_setup.set_channel(playback_name + '_MARKER_FRONT', MarkerChannel(channels, 2 * ch_i))
        hardware_setup.set_channel(playback_name + '_MARKER_BACK', MarkerChannel(channels, 2 * ch_i + 1))
    
    return hdawg


def get_mds_group(hardware_setup, serials: Sequence[str], device_interface='USB'):
    """First serial has to be the MDS master"""
    hdawgs = []
    
    for serial in serials:
        hdawgs.append(HDAWGRepresentation(serial, device_interface=device_interface, grouping=HDAWGChannelGrouping.MDS))
    
    # bug?
    for hdawg in hdawgs:
        hdawg.channel_grouping = HDAWGChannelGrouping.MDS
    
    channels, = hdawgs[0].channel_tuples
    
    for awg_idx, hdawg in enumerate(hdawgs):
        for ch_idx_on_awg, ch_name in enumerate('ABCDEFGH'):
            ch_idx = ch_idx_on_awg + 8 * awg_idx
            playback_name = f'ZI{awg_idx}_{ch_name}'
            hardware_setup.set_channel(playback_name, PlaybackChannel(channels, ch_idx))
            hardware_setup.set_channel(playback_name + '_MARKER_FRONT', MarkerChannel(channels, 2 * ch_idx))
            hardware_setup.set_channel(playback_name + '_MARKER_BACK', MarkerChannel(channels, 2 * ch_idx + 1))
    return hdawgs


# class MuxAwg(Muxi):
    
#     def __init__(self, name, address, BAUD,awg_gates, **kwargs):
        
#         super().__init__(name, address, BAUD, **kwargs)
        
#         self.awg_gates=awg_gates
#         self.state=[0,0,0,0]
        
#     def connect(self,gate):
        
#         for i in range(len(self.state)):
            
#             if self.state[i]==gate:
                
#                 return(self.awg_gates[i])

#         for i in range(len(self.state)):
            
#             if self.state[i]==0:
                
#                 self.state[i]=gate
#                 self.parameters[gate+'_0'+str(i+1)].set(1)
                
#                 return(self.awg_gates[i])
        
#         print('Found no free slot')
    
#     def disconnect(self,gate):
        
#         for i in range(len(self.state)):
            
#             if self.state[i] is gate:
#                 self.parameters[gate+'_0'+str(i+1)].set(0)
#                 self.state[i] = 0
    
#     def reset(self):
        
#         super().full_reset()
#         self.state=[0,0,0,0]
        
class MuxAwg:
    
    def __init__(self,*mux_serial:Sequence):
    
        self.muxes=[]
        self.awg_gates=[]
        self.state=[]
        self.state_to_be_changed=0
        for mux in mux_serial:
            
            self.add_mux(mux[0],mux[1])
            
    def add_mux(self,serial,channels):
        
        self.muxes.append(Muxi('mux_'+str(len(self.muxes)),serial,115200))
        self.awg_gates+=channels
        self.state+=[0 for entry in channels]
        
    def connect(self,gate):
        
        for i in range(len(self.state)):
            if self.state[i]==gate:
                return(self.awg_gates[i])
        
        if self.state[self.state_to_be_changed]:
            self.muxes[self.state_to_be_changed//4].parameters[r'C{:02d}_{:02d}'.format(self.state[self.state_to_be_changed],self.state_to_be_changed%4+1)].set(0)
        
        self.muxes[self.state_to_be_changed//4].parameters[r'C{:02d}_{:02d}'.format(gate,self.state_to_be_changed%4+1)].set(1)
        self.state[self.state_to_be_changed]=gate
        awg_gate=self.awg_gates[self.state_to_be_changed]
        self.state_to_be_changed+=1
        if self.state_to_be_changed==len(self.state):
            self.state_to_be_changed=0   
            
        return awg_gate
        
    def reset(self):
        
        for mux in self.muxes:
            mux.full_reset()
        self.state[:]=[0 for state in self.state]
    
        
class MuxChan:
    
    def __init__(self, string, Mux=None):

        self.string=string
        self.Mux=Mux
    
    def __repr__(self):
        
        return self.Mux.connect(self.string)
        
    
    
    
            
            
                
                
    
    
    
        
        
        
        
        
        
    



    