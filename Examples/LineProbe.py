import numpy as np 
import matplotlib.pyplot as plt
from mux_qcodes_driver import Muxi
from mux_chainer_map import mux_to_chainer_map
from adapters import adapters

serial = 'ASRL12::INSTR'
mux = Muxi('mux',serial,115200) #instrument name, serial, baud rate

#%%
def get_resistances(mux,gnd,volt,rfix,adapter):
    resistances = mux.ask('LINEPROBE:RUN '+str(gnd)+','+str(volt)+','+str(rfix))
    if adapter:
        inputs = adapters(adapter)
    return inputs, resistances
    
#%%
plt.figure()

while True:
   plt.clf()
   inputs,resistances = get_resistances(mux,1,1.0,100,'mux_chainer')
   plt.plot(inputs,resistances,'.',color = 'maroon')
   plt.xlabel('input channel')
   plt.ylabel(r'restsiance $\Omega$')
   plt.pause(0.5) 