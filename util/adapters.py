import numpy as np

def adapters(adapter):
    if adapter == 'mux_chainer':
        return [i//2+i%2*50+1 for i in range(96)]